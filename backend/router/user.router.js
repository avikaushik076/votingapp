const express = require('express');
const router = express.Router();

const { userSignupData, userLoginData, userVote, candidateInfo } = require('../controller/user.controller');
const auth = require('../middleware/auth');


router.post("/signup", userSignupData)
router.post("/login", userLoginData)
router.post("/vote",auth, userVote)
router.get("/vote", candidateInfo)

module.exports = router;
