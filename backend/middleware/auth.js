const jwt = require("jsonwebtoken");

function auth (req, res, next){
    try{
        let token = req.headers.authorization;
        if(token){
            token=token.split(" ")[1];
            let user = jwt.verify(token, process.env.SECRET_KEY);
                req.id=user.id            
        }
        else{
            res.send("Unauthorized user");
        }
        next()
    }
    

    catch (err) {
        console.log(err)
    res.send("Error")  
    
    }
      
}
module.exports = auth ;
