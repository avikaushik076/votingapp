
function userSignup(name, id, password) {
    return new Promise((resolve, reject) => {
        const sql = `INSERT INTO voters(name, id, password) VALUES ("${name}","${id}","${password}")`;
        global.db.query(sql, function (err, result) {
            if (err) {

                reject(err);
            }
            resolve(result)
            console.log("User record inserted");
        });
    })
}


function userLogin(id, password) {
    return new Promise((resolve, reject) => {
        const sql = `select id , password from voters WHERE id = ${id} AND password = ${password}`;
        global.db.query(sql, function (err, result) {
            if (err) {

                reject(err)
            }
            resolve(result)
            console.log("User record fetched");
        });
    })
}

function userDecision(userid, candidateid) {
    return new Promise((resolve, reject) => {
        const sql = `INSERT INTO votedUsers(userid, candidateid) VALUES ("${userid}","${candidateid}")`
        global.db.query(sql, function (err, result) {
            if (err) {

                reject(err)
            }
            resolve(result)
            console.log("Vote Submitted");
        });

    })
}

function candidateRecord() {
    return new Promise((resolve, reject) => {
        const sql = `select * from Candidates`;
        global.db.query(sql, function (err, result) {
            if (err) {

                reject(err)
            }
            resolve(result)
            console.log("Candidate record fetched");
        });
    })
}

module.exports = { userSignup, userLogin, userDecision, candidateRecord }

