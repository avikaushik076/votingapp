const { userSignup, userLogin, userDecision, candidateRecord } = require('../model/user.model');
const path = require('path')
const { successMessage } = require('../utility/responseMessage')
const jwt = require("jsonwebtoken")

async function userSignupData(req, res, next) {
  try {
    let { name, id, password } = req.body;
    console.log({ name, id, password })

    await userSignup(name, id, password)
    res.send(successMessage.loginSuccess)

  } catch (err) {
    console.log(err)
  }
  next();
}

async function userLoginData(req, res, next) {
  try {
    let { id, password } = req.body
    const data = await userLogin(id, password)
    if (data == "") {
      res.send({ "message": "invalid user" })
    }
    else {
      const token = jwt.sign(id,process.env.SECRET_KEY)
      res.header('Authorization', 'Bearer ' + token);
      res.send({"message":"valid user "})

    }
  } catch (err) {
    res.send({"message":err})
    console.log(err)
  }
  next()
}


async function userVote(req, res, next) {
  try {
    let { userid, candidateid } = req.body;
    console.log({ userid, candidateid })

    await userDecision(userid, candidateid)
    res.send(successMessage.loginSuccess)

  } catch (err) {
    console.log(err)
  }
  next();
}
async function candidateInfo(req, res, next) {
  try {

    const candidate = await candidateRecord()
    console.log(candidate)
    res.send("data fetched")
    next()
  } catch (err) {
    console.log(err)
  }

}



module.exports = { userSignupData, userLoginData, userVote, candidateInfo }
