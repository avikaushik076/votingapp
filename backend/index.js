const express = require('express');
const app = express();
const path = require('path')
const bodyParser = require('body-parser')
const userRouter = require('./router/user.router')
const { connectSQl } = require('./utility/db')
connectSQl();
app.use(bodyParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use((req, res, next)=>{
    console.log("HTTP METHOD - " + req.method + "  " + "URL - " + req.url)
    next()
})

app.use('/user', userRouter);


app.listen(4001, '0.0.0.0', () => console.log('Example app listening on port 4001!'));
